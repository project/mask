(function ($, Drupal, drupalSettings, once) {
  // Disables masking by data-mask attribute.
  $.jMaskGlobals.dataMask = false;

  // Use modules settings for digit translation.
  $.jMaskGlobals.translation = {};

  // Declare translation variables and apply them.
  const translation = drupalSettings.mask.translation;
  Object.keys(translation).forEach((symbol) => {
    const options = translation[symbol];
    options.pattern = new RegExp(options.pattern);
    $.jMaskGlobals.translation[symbol] = options;
  });
  function maskElements() {
    const maskElement = $(this);

    // Get mask options.
    const maskValue = maskElement.attr('data-mask-value');
    const maskOptions = {
      reverse: maskElement.attr('data-mask-reverse') === 'true',
      clearIfNotMatch: maskElement.attr('data-mask-clearifnotmatch') === 'true',
      selectOnFocus: maskElement.attr('data-mask-selectonfocus') === 'true',
      translation: drupalSettings.mask.translation,
    };

    // Apply the mask.
    maskElement.mask(maskValue, maskOptions);
  }

  Drupal.behaviors.mask = {
    attach(context, settings) {
      // Get mask fields and apply mask.
      $(once('mask', '[data-mask-value]', context)).each(maskElements);
    },
  };
})(jQuery, Drupal, drupalSettings, once);
